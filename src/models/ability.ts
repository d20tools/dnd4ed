import {number} from "@treacherous/decorators";

/**
 * @constructor Ability
 * Contains all details that make up an ability within the system
 */
export class Ability
{
    /**
     * @object AbilityType
     * The ability type
     */
    @number()
    public AbilityType = 0;

    /**
     * @object TotalScore
     * The total score of the ability
     */
    @number()
    public TotalScore = 0;

    /**
     * @object ModifierScore
     * The modifier score of the ability
     */
    @number()
    public ModifierScore = 0;

    /**
     * @object BaseScore
     * The base score of the ability
     */
    @number()
    public BaseScore = 0;

    /**
     * @object MiscBonus
     * The misc bonus score of the ability
     */
    @number()
    public MiscBonus = 0;
}