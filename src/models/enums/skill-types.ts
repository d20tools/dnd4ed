/**
 * @object SkillTypes
 * An enum-like object for encapsulating action types and mappers from type to text
 *
 * @prop [Number] Acrobatics The acrobatics skill type
 * @prop [Number] Arcana The arcana action type
 * @prop [Number] Athletics The athletics action type
 * @prop [Number] Bluff The bluff action type
 * @prop [Number] Diplomacy The diplomacy action type
 * @prop [Number] Dungeoneering The dungeoneering action type
 * @prop [Number] Endurance The endurance action type
 * @prop [Number] Heal The heal action type
 * @prop [Number] History The history action type
 * @prop [Number] Insight The insight action type
 * @prop [Number] Intimidate The intimidate action type
 * @prop [Number] Nature The nature action type
 * @prop [Number] Perception The perception action type
 * @prop [Number] Religion The religion action type
 * @prop [Number] Stealth The stealth action type
 * @prop [Number] Streetwise The streetwise action type
 * @prop [Number] Thievery The thievery action type
 */
export class SkillTypes
{
    public static Acrobatics = 1;
    public static Arcana = 2;
    public static Athletics = 3;
    public static Bluff = 4;
    public static Diplomacy = 5;
    public static Dungeoneering = 6;
    public static Endurance = 7;
    public static Heal = 8;
    public static History = 9;
    public static Insight = 10;
    public static Intimidate = 11;
    public static Nature = 12;
    public static Perception = 13;
    public static Religion = 14;
    public static Stealth = 15;
    public static Streetwise = 16;
    public static Thievery = 17;
}