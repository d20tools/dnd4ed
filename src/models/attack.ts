import {required, minLength, maxLength, number} from '@treacherous/decorators';

/**
 * @constructor Attack
 * Contains all details that make up an attack/weapon within the system
 */
export class Attack
{
    /**
     * @object WeaponName
     * The name of the weapon
     */
    @required()
    @minLength(2)
    @maxLength(50)
    public WeaponName = "";

    /**
     * @object Damage
     * The damage the weapon does, usually measured in dice rolls i.e. 2d6, 3d10+2 etc
     */
    @maxLength(30)
    public Damage = "";

    /**
     * @object Range
     * The range of the weapon, usually measured in squares i.e. 2 squares
     */
    @maxLength(30)
    public Range = "";

    /**
     * @object ToHit
     * The to hit score for the weapon
     */
    @number()
    public ToHit = 0;

    /**
     * @object ModifierBonus
     * The modifier bonus
     */
    @number()
    public ModifierBonus = 0;

    /**
     * @object ProficiencyBonus
     * The proficiency bonus from the weapon
     */
    @number()
    public ProficiencyBonus = 0;

    /**
     * @object HalfLevelBonus
     * The half level bonus
     */
    @number()
    public HalfLevelBonus = 0;

    /**
     * @object FeatsBonus
     * The to feat related bonuses
     */
    @number()
    public FeatsBonus = 0;

    /**
     * @object EnhancementBonus
     * The enhancement bonus
     */
    @number()
    public EnhancementsBonus = 0;

    /**
     * @object MiscBonus
     * The misc bonus
     */
    @number()
    public MiscBonus = 0;
}