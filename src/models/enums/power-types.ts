/**
 * @object PowerTypes
 * An enum-like object for encapsulating power types and mappers from type to text
 *
 * @prop [Number] AtWill The at will power type
 * @prop [Number] Encounter The encounter power type
 * @prop [Number] Daily The daily power type
 */
export class PowerTypes
{
    public static AtWill = 1;
    public static Encounter = 2;
    public static Daily = 3;
}