import {number} from "@treacherous/decorators";

/**
 * @constructor Speed
 * Contains information that describes a skill
 */
export class Speed
{
    /**
     * @object TotalScore
     * The total score for speed
     *
     * @note This is basically how far you can move each go
     */
    @number()
    public TotalScore = 0;

    /**
     * @object RaceBonus
     * The race bonus applied to speed
     */
    @number()
    public RaceBonus = 0;

    /**
     * @object ArmourBonus
     * The armour bonus applied to speed
     */
    @number()
    public ArmourBonus = 0;

    /**
     * @object MiscBonus
     * The misc bonus applied to speed
     */
    @number()
    public MiscBonus = 0;
}
