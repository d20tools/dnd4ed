/**
 * @object ActionTypes
 * An enum-like object for encapsulating action types and mappers from type to text
 *
 * @prop [Number] Minor The minor action type
 * @prop [Number] Movement The movement action type
 * @prop [Number] Standard The standard action type
 * @prop [Number] FreeAction The free action action type
 * @prop [Number] Reaction The reaction action type
 */
export class ActionTypes
{
    public static Minor = 1;
    public static Movement = 2;
    public static Standard = 3;
    public static FreeAction = 4;
    public static Reaction = 5;
}