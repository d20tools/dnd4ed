import {number} from "@treacherous/decorators"

/**
 * @constructor Skill
 * Contains information that describes a skill
 */
export class Skill
{
    /**
     * @object Name
     * The name of the skill
     */
    @number()
    public SkillType = 0;

    /**
     * @object TotalScore
     * The total score of the skill
     */
    @number()
    public TotalScore = 0;

    /**
     * @object Trained
     * A boolean indicating if the character is trained in this skill
     */
    public Trained = false;

    /**
     * @object HalfLevelBonus
     * The half level bonus applied
     */
    @number()
    public HalfLevelBonus = 0;

    /**
     * @object AbilityBonus
     * The name of the skill
     */
    @number()
    public AbilityBonus = 0;

    /**
     * @object SkillFocus
     * The amount of times the player has focused in this skill
     *
     * @note This may end up changing to a boolean like IsTrained
     */
    @number()
    public SkillFocus = 0;

    /**
     * @object MiscBonus
     * The misc bonus applied to this skill
     */
    @number()
    public MiscBonus = 0;
}