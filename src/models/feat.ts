import {required, maxLength} from '@treacherous/decorators';

/**
 * @constructor Feat
 * Contains feat related details
 *
 */
export class Feat
{
    /**
     * @object Name
     * The name of the feat
     */
    @required()
    @maxLength(50)
    public Name = "";

    /**
     * @object Prerequisites
     * Any prerequisites for this feat
     */
    @maxLength(500)
    public Prerequisites = "";

    /**
     * @object Benefits
     * The benefits of this feat
     */
    @maxLength(500)
    public Benefits = "";
}
