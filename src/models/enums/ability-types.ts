/**
 * @object AbilityTypes
 * An enum-like object for encapsulating ability types and mappers from type to text
 *
 * @prop [Number] Strength The strength ability type
 * @prop [Number] Dexterity The dexterity ability type
 * @prop [Number] Constitution The constitution ability type
 * @prop [Number] Intelligence The intelligence ability type
 * @prop [Number] Wisdom The wisdom ability type
 * @prop [Number] Charisma The charisma ability type
 */
export class AbilityTypes
{
    public static Strength = 1;
    public static Dexterity = 2;
    public static Constitution = 3;
    public static Intelligence = 4;
    public static Wisdom = 5;
    public static Charisma = 6;
}
