import {number} from "@treacherous/decorators";

/**
 * @constructor Currency
 * Contains currency related details
 *
 */
export class Currency
{
    /**
     * @object PlatinumCoins
     * The total initiative score
     */
    @number()
    public PlatinumCoins = 0;

    /**
     * @object GoldCoins
     * The amount of gold coins in the players posession
     */
    @number()
    public GoldCoins = 0;

    /**
     * @object SilverCoins
     * The amount of silver coins in the players posession
     */
    @number()
    public SilverCoins = 0;

    /**
     * @object CopperCoins
     * The amount of copper coins in the players posession
     */
    @number()
    public CopperCoins = 0;

}

