import {required, maxLength, minValue, number} from '@treacherous/decorators';

/**
 * @constructor Details
 * Contains core character details
 */
export class Details
{
    /**
     * @object Race
     * The characters race
     */
    @required()
    @maxLength(30)
    public Race = "";

    /**
     * @object Alignment
     * The characters alignment
     */
    @maxLength(30)
    public Alignment = "";

    /**
     * @object Deity
     * The characters deity
     */
    @maxLength(30)
    public Deity = "";

    /**
     * @object Class
     * The characters class
     */
    @maxLength(50)
    public Class = "";

    /**
     * @object ParagonClass
     * The characters paragon class
     */
    @maxLength(50)
    public ParagonClass = "";

    /**
     * @object EpicClass
     * The characters epic class
     */
    @maxLength(50)
    public EpicClass = "";

    /**
     * @object Level
     * The characters level
     */
    @number()
    @minValue(1)
    public Level = 1;

    /**
     * @object Experience
     * The characters experience
     */
    @number()
    public Experience = 0;

    /**
     * @object NextLevelExperience
     * The characters experience goal for next level
     */
    @number()
    public NextLevelExperience = 0;

    /**
     * @object ActionPoints
     * The characters action points
     */
    @number()
    public ActionPoints = 0;
}