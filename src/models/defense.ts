import {number} from "@treacherous/decorators";

/**
 * @constructor Defense
 * Contains defense based information
 */
export class Defense
{
    /**
     * @object DefenseType
     * The defense type for this instance
     *
     * @Note This should use DefenseTypes enum
     */
    @number()
    public DefenseType: number;

    /**
     * @object TotalScore
     * The total score of the defense
     */
    @number()
    public TotalScore = 0;

    /**
     * @object BaseAndHalfLevelBonus
     * The base + half level bonus
     */
    @number()
    public BaseAndHalfLevelBonus = 0;

    /**
     * @object AbilityBonus
     * The ability related bonus
     */
    @number()
    public AbilityBonus = 0;

    /**
     * @object ClassBonus
     * The class related bonus
     */
    @number()
    public ClassBonus = 0;

    /**
     * @object FeatsBonus
     * The feats related bonus
     */
    @number()
    public FeatsBonus = "";

    /**
     * @object EnhancementsBonus
     * The enhancements related bonus
     */
    @number()
    public EnhancementsBonus = 0;

    /**
     * @object MiscBonus
     * The misc bonus to apply
     */
    @number()
    public MiscBonus = 0;
}
