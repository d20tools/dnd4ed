import {number, maxLength} from "@treacherous/decorators"

/**
 * @constructor Health
 * Contains health/status related details
 */
export class Health
{
    /**
     * @object MaxHealth
     * The characters maximum health
     */
    @number()
    public MaxHealth = 0;

    /**
     * @object Health
     * The characters current health
     */
    @number()
    public CurrentHealth = 0;

    /**
     * @object BloodiedValue
     * The characters bloodied value
     */
    @number()
    public BloodiedValue = 0;

    /**
     * @object HealingSurges
     * The characters healing surges available for this character
     */
    @number()
    public HealingSurges = 0;

    /**
     * @object HealingSurgeValue
     * The amount of health regained by a healing surge
     */
    @number()
    public HealingSurgeValue = 0;

    /**
     * @object HealingSurgesUsed
     * The amount of healing surges used
     */
    @number()
    public HealingSurgesUsed = 0;

    /**
     * @object SecondWindUsed
     * Boolean indicating if the character has used their second wind
     */
    public SecondWindUsed = false;

    /**
     * @object SavingThrowFailOne
     * The first failed saving throw
     */
    public SavingThrowFailOne = false;

    /**
     * @object SavingThrowFailTwo
     * The second failed saving throw
     */
    public SavingThrowFailTwo = false;

    /**
     * @object SavingThrowFailThree
     * The third failed saving throw
     */
    public SavingThrowFailThree = false;

    /**
     * @object Status
     * The characters status
     */
    @maxLength(500)
    public Status = "";
}