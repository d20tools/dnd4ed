import {required, maxLength} from '@treacherous/decorators';

/**
 * @constructor Ritual
 * Contains information that describes a ritual
 */
export class Ritual
{
    /**
     * @object Name
     * The name of the ritual
     */
    @required()
    @maxLength(50)
    public Name = "";

    /**
     * @object Description
     * The description of the rituals effects
     */
    @maxLength(500)
    public Description = "";

    /**
     * @object Reagents
     * The reagents required to use the ritual
     */
    @maxLength(500)
    public Reagents = "";

    /**
     * @object Preparation
     * The preparation steps to use the ritual
     */
    @maxLength(500)
    public Preparation = "";
}