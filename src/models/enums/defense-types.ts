/**
 * @object DefenseTypes
 * An enum-like object for encapsulating defense types and mappers from type to text
 *
 * @prop [Number] ArmourClass The armour class defense type
 * @prop [Number] Fortitude The fortitude defense type
 * @prop [Number] Reflex The reflex defense type
 * @prop [Number] Will The will defense type
 */
export class DefenseTypes
{
    public static ArmourClass = 1;
    public static Fortitude = 2;
    public static Reflex = 3;
    public static Will = 4;
}