import {maxLength, required, number} from '@treacherous/decorators';

/**
 * @constructor Power
 * Contains information that describes a power
 */
export class Power
{
    /**
     * @object Name
     * The name of the power
     */
    @required()
    @maxLength(50)
    public Name = "";

    /**
     * @object Keywords
     * The powers keywords
     */
    @maxLength(100)
    public Keywords = "";

    /**
     * @object PowerType
     * The type of the power
     *
     * @note Will be of PowerTypes enum
     */
    @number()
    public PowerType = 0;

    /**
     * @object ActionType
     * The action type of the power
     *
     * @note Will be of ActionTypes enum
     */
    @number()
    public ActionType = 0;

    /**
     * @object Damage
     * The damage of the power, normally in the format of a dice roll i.e. 2d6
     */
    @number()
    public Damage = 0;

    /**
     * @object Range
     * The range of the power, normally in the format of squares i.e. 2 squares
     */
    @maxLength(50)
    public Range = "";

    /**
     * @object Target
     * The target of the power, usually either self, group, 3 squares
     */
    @maxLength(50)
    public Target = "";

    /**
     * @object Attack
     * The attack score of the power
     */
    @maxLength(50)
    public Attack = "";

    /**
     * @object Description
     * The description for this power
     */
    @maxLength(1000)
    public Description = "";

    /**
     * @object Effect
     * The description for this powers effects
     */
    @maxLength(1000)
    public Effect = "";
}