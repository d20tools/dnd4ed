import {DefaultCharacter, Language, Inventory, Equipment, SystemTypes} from "@d20tools/characters";
import {Details} from "./details";
import {Defense} from "./defense";
import {Health} from "./health";
import {Initiative} from "./initiative";
import {Skill} from "./skill";
import {Ability} from "./ability";
import {Speed} from "./speed";
import {Currency} from "./currency";
import {Attack} from "./attack";
import {Power} from "./power";
import {Feat} from "./feat";
import {Ritual} from "./ritual";
import {withRuleset, withRulesetForEach} from '@treacherous/decorators';

/**
 * @constructor Character
 * Represents a Dnd4ed character, this is a composition of all other models
 */
export class Character extends DefaultCharacter
{
    public static LatestVersion = "2.0.0";

    /**
     * @object Version
     * The current version of this model
     */
    public Version: string = Character.LatestVersion;

    /**
     * @object SystemType
     * The system type of the character
     */
    public SystemType: number = SystemTypes.Dnd4ed;

    /**
     * @object Details
     * The character details model
     */
    @withRuleset(Details)
    public Details = new Details();

    /**
     * @object Abilities
     * The character abilities array
     *
     * @note This array is observable in nature and will contain ability models
     */
    @withRulesetForEach(Ability)
    public Abilities: Ability[] = [];

    @withRulesetForEach(Language)
    public Languages: Language[] = [];

    /**
     * @object Defenses
     * The character defenses array
     *
     * @note This array is observable in nature and will contain defense models
     */
    @withRulesetForEach(Defense)
    public Defenses: Defense[] = [];

    /**
     * @object Skills
     * The character skills array
     *
     * @note This array is observable in nature and will contain skill models
     */
    @withRulesetForEach(Skill)
    public Skills: Skill[] = [];

    /**
     * @object Health
     * The health model
     */
    @withRuleset(Health)
    public Health = new Health();

    /**
     * @object Initiative
     * The initiative model
     */
    @withRuleset(Initiative)
    public Initiative = new Initiative();

    /**
     * @object Speed
     * The speed model
     */
    @withRuleset(Speed)
    public Speed = new Speed();

    /**
     * @object Inventory
     * The inventory model
     */
    @withRuleset(Inventory)
    public Inventory = new Inventory();

    /**
     * @object Currency
     * The currency model
     */
    @withRuleset(Currency)
    public Currency = new Currency();

    /**
     * @object Equipment
     * The equipment model
     */
    @withRuleset(Equipment)
    public Equipment = new Equipment();

    /**
     * @object Attacks
     * The character attacks array
     *
     * @note This array is observable in nature and will contain attack models
     */
    @withRuleset(Attack)
    public Attacks: Attack[] = [];

    /**
     * @object Powers
     * The character powers array
     *
     * @note This array is observable in nature and will contain power models
     */
    @withRulesetForEach(Power)
    public Powers: Power[] = [];

    /**
     * @object Feats
     * The character feats array
     *
     * @note This array is observable in nature and will contain feats models
     */
    @withRulesetForEach(Feat)
    public Feats: Feat[] = [];

    /**
     * @object Rituals
     * The character rituals array
     *
     * @note This array is observable in nature and will contain ritual models
     */
    @withRulesetForEach(Ritual)
    public Rituals: Ritual[] = [];
}