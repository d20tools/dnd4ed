import {ICharacterFactory, ICharacter, createTypesFor} from "@d20tools/characters";
import {Character} from "../models/character"
import {Ability} from "../models/ability";
import {AbilityTypes} from "../models/enums/ability-types";
import {Defense} from "../models/defense";
import {DefenseTypes} from "../models/enums/defense-types";
import {Skill} from "../models/skill";
import {SkillTypes} from "../models/enums/skill-types";

/**
 * @constructor CharacterFactory
 * Deals with creating a new character fully populated
 */
export class CharacterFactory implements ICharacterFactory
{
    public CurrentVersion = Character.LatestVersion;

    /**
     * @method .CreateAbilities
     * Creates the default abilities with default values
     *
     * @private
     */
    private CreateAbilities(): Ability[]
    {
        return createTypesFor(AbilityTypes, 0, Ability, (model, type, key) => {
            model.AbilityType = type;
        });
    }

    /**
     * @method .CreateDefenses
     * Creates default defenses with default values
     *
     * @private
     */
    private CreateDefenses(): Defense[]
    {
        return createTypesFor(DefenseTypes, 0, Defense, (model, type, key) => {
            model.DefenseType = type;
        });
    }

    /**
     * @method .CreateSkills
     * Creates the default skills with default values
     *
     * @private
     */
    private CreateSkills(): Skill[]
    {
        return createTypesFor(SkillTypes, 0, Skill, (model, type, key) => {
            model.SkillType = type;
        });
    }

    /**
     * @method .Create
     * Creates the default character with default values
     *
     * @public
     */
    public create():ICharacter {
        const character = new Character();
        character.Abilities = this.CreateAbilities();
        character.Defenses = this.CreateDefenses();
        character.Skills = this.CreateSkills();
        return character;
    }
}