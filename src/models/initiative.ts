import {number} from "@treacherous/decorators"

/**
 * @constructor Initiative
 * Contains character initiative related details
 */
export class Initiative
{
    /**
     * @object TotalScore
     * The total initiative score
     */
    @number()
    public TotalScore = 0;

    /**
     * @object HalfLevelBonus
     * The half level bonus applied
     */
    @number()
    public HalfLevelBonus = 0;

    /**
     * @object AbilityBonus
     * The ability bonus applied
     */
    @number()
    public AbilityBonus = 0;

    /**
     * @object MiscBonus
     * The misc bonus applied
     */
    @number()
    public MiscBonus = 0;
}